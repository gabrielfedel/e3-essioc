# CHANGELOG
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Links
For details of changes, refer to the changelogs for the included modules:
* [`auth`](https://gitlab.esss.lu.se/e3/wrappers/core/e3-auth/-/blob/master/CHANGELOG.md)
* [`autosave`](https://gitlab.esss.lu.se/e3/wrappers/core/e3-autosave/-/blob/master/CHANGELOG.md)
* [`caputlog`](https://gitlab.esss.lu.se/e3/wrappers/core/e3-caputlog/-/blob/master/CHANGELOG.md)
* [`iocstats`](https://gitlab.esss.lu.se/e3/wrappers/core/e3-iocstats/-/blob/master/CHANGELOG.md)
* [`recsync`](https://gitlab.esss.lu.se/e3/wrappers/core/e3-recsync/-/blob/master/CHANGELOG.md)

## [Unreleased]

## [0.6.6]
### Other changes
* Update auth to 0.2.7

## [0.5.0]
### New Features
* Add documentation with usage information
### Bugfixes
* Pass IOCDIR to autosave snippet
### Other changes
* Updated auth from 0.1.0 to 0.2.0

## [0.4.1]
### New Features
### Bugfixes
### Other changes
Renamed from e3-common
